import nmap
import time

nm = nmap.PortScanner()
start = time.time()
nm.scan(hosts="10.0.0-1.0-255", arguments="--resolve-all --verbose -FA")
end = time.time()
print("LONG SCAN TIME:\t")
print(end-start)
print(nm.all_hosts())
start = time.time()
nm.scan(hosts="10.0.0-1.0-255", arguments="-sn --verbose")
end = time.time()
print("SHORT SCAN TIME:\t")
print(end-start)
print(nm.all_hosts())

