import nmap
import pandas as pd
import requests
import argparse
import os
import sqlite3
import json
import base64
import uuid
import subprocess

from dash import dash_table
import dash
import dash_core_components as dcc    
import dash_html_components as html

def get_public_ip():
    endpoint = 'https://ipinfo.io/json'
    response = requests.get(endpoint, verify = True)

    if response.status_code != 200:
        return 'Status:', response.status_code, 'Problem with the request. Exiting.'
        exit()

    data = response.json()

    return data['ip']

parser = argparse.ArgumentParser()
parser.add_argument("-r", "--range", help="Range of ip's to scan", type=str)
parser.add_argument("-l", "--location", help="Specify location of data collection", type=str)
parser.add_argument("-s", "--scan", help="Scan the current network", action="store_true")
parser.add_argument("-a", "--analysis", help="Scan the current network", action="store_true")
args=parser.parse_args()
location_prep = get_public_ip()
if not args.location == None:
    location_prep = [location_prep, args.location]
network_name=os.popen("sudo iwgetid -r").read()
session_id = str(uuid.uuid4())




def isolate_nationality_from_hostname(hostname):
    hostname = hostname.lower()
    hostname_s = hostname.replace("-", " ").replace("_", " ").split(" ")
    nationality_matrix = {
        "German": len(hostname_s) > 2 and hostname_s[1] == "von"
    }
    return [nat for nat in nationality_matrix if nationality_matrix[nat] == True]

def isolate_name_from_hostname(hostname, nationality):
    hostname_s = hostname.replace("-", " ").replace("_", " ").split(" ")
    name_location_matrix = {
        "German": 2
    }
    return [hostname_s[index] for index in [name_location_matrix[nation] for nation in nationality]]




def network_inteligence_scan(db, report_action=1):
    nm = nmap.PortScanner()
    nm.scan(hosts=args.range, arguments="--resolve-all --verbose -FAO")
    ip_hostname=[(client, nm[str(client)]['hostnames'][0]['name']) for client in nm.all_hosts()]
    for pair in ip_hostname:
        if len(pair[1]) > 0:
            print(pair)
            nat=isolate_nationality_from_hostname(pair[1])
            full=nm[str(pair[0])]
            full_has_mac = "mac" in full["addresses"]
            mac=None
            os = None
            if len(full['osmatch']) > 0:
                os_rep = full['osmatch'][0]
                os = [os_rep['name'], os_rep['accuracy']]
            if full_has_mac:
                mac = full['addresses']['mac']
            report={
                "data": {
                    "ip": pair[0],
                    "mac": mac,
                    "hostname": pair[1],
                    "dump" : full
                },
                "intel" : {
                    "nationality" :  nat,
                    "user_name" : isolate_name_from_hostname(pair[1], nat)
                }
            }

            # save to db
            data=str(full)
            urlSafeEncodedBytes = base64.urlsafe_b64encode(data.encode("utf-8"))
            urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
            payload=f"insert into `devices` (mac, ip, network_name, session_id, session_location, hostname, operating_system, nationality, user_name, full_report) values (\"{report['data']['mac']}\",\"{report['data']['ip']}\",\"{network_name}\", \"{session_id}\",\"{location_prep}\", \"{report['data']['hostname']}\", \"{str(os)}\", \"{report['intel']['nationality']}\",\"{report['intel']['user_name']}\",\"{urlSafeEncodedStr}\")"
            if report_action==1:
                print(report)
            else:
                continue
            db.execute(payload)
            db.commit()


def identify_unique_devices(data):
    unique_device_groups = list()
    for row in data:
        matches=[row]
        for row1 in data:
            row1_not_row = not row1 == row
            rows_match = row1[7] == row[7]
            if rows_match and row1_not_row:
                matches.append(row1)
        print([match[:5] for match in matches])
        for match in matches:
            data.remove(match)
        unique_device_groups.append(matches)
    return unique_device_groups


def open_network_inteligence_dashboard(db):
    app = dash.Dash()

    dbb= db
    db = dbb.cursor()
    db.execute("select * from devices;")
    dbb.commit()
    all_data=db.fetchall()
    session_directory = dict()
    
    for row in all_data:
        if row[5] in session_directory.keys():
            session_directory[row[5]] += 1
        else:
            session_directory[row[5]] = 1


    found_os_directory = {
        "None": 0
    }
    #8
    for row in all_data:
        print(row[8])
        if not row[8] == "None":
            os_list=json.loads(row[8].replace("'", "\""))
            os_first=os_list[0].split(" ")[0]
            if os_first in found_os_directory.keys():
                found_os_directory[os_first] += 1
            else:
                found_os_directory[os_first] = 1
        else:
            found_os_directory["None"] += 1



    print(found_os_directory)

    UD_groups = identify_unique_devices(all_data)
    UD_groups_directory = list()

    def present_list(lst):
        return "|".join(list(dict.fromkeys(lst)))

    def present_rows_element(index, group):
        return present_list([sg[index] for sg in group])
    
    for group in UD_groups:
        # this could be refactored even more...im tired and lazy
        UD_groups_directory.append({
            "Hostname": group[0][7],
            "MAC": group[0][1],
            "IPs": present_rows_element(2, group),
            "Contanct Networks": present_rows_element(4, group),
            "Times seen": present_rows_element(3, group),
            "Guessed Nationality": present_rows_element(9, group),
            "Guessed Name": present_rows_element(10, group)
        })
    print(UD_groups_directory)

    app.layout = html.Div(children =[
        html.H1("Session device discovery"),
        dcc.Graph(
            id ="device_n",
            figure ={
                'data':[
                        {'x':[i for i in session_directory],
                            'y':[session_directory[i] for i in session_directory],
                            'type':'bar', 
                            'name':'Devices'}
                    ],
                'layout':{
                    'title':'number of devices found'
                }
            }
        ),
        dcc.Graph(
            figure = {
                'data': [
                    {
                        'values': [found_os_directory[i] for i in found_os_directory.keys()],
                        'labels': [i for i in found_os_directory],
                        'type': 'pie',
                        'name': "OS"
                    }
                ],
                'layout': {
                    'title': "Operating System of devices found [all-time]"
                }
            }
        ),
        dash_table.DataTable(data=UD_groups_directory)
    ])
    app.run_server(debug=True)
            
def db_connect():
    conn=sqlite3.connect("neinga.db")
    print("connection to database open")
    return conn

def db_create_main_table(conn):
    string="""
CREATE TABLE IF NOT EXISTS `devices` (
    `id` INTEGER PRIMARY KEY,
	`mac` VARCHAR,
	`ip` TEXT(20),
	`discovery` DATETIME DEFAULT CURRENT_TIMESTAMP,
	`network_name` TEXT(1024),
    `session_id` VARCHAR(1024),
    `session_location` varchar(2048),
	`hostname` VARCHAR(1024),
    `operating_system` VARCHAR(2048),
	`nationality` VARCHAR(1024),
	`user_name` VARCHAR(1024),
	`full_report` varchar(1600)
);
    """
    conn.execute(string)

gconn = db_connect()
db_create_main_table(gconn)

if args.scan == True:
    network_inteligence_scan(gconn)
elif args.analysis == True:
    print("")
    open_network_inteligence_dashboard(gconn)
else:
    print("Err: you must select an action to execute: scan/analysis")

gconn.close()
